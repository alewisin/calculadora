package Ventana;

import junit.framework.TestCase;

public class TestPrincipal extends TestCase {

	// Metodo suma
	public void testSuma() {
		assertEquals(20, Operaciones.suma(10));
	}

	// Metodo resta
	public void testResta() {
		assertEquals(0, Operaciones.resta(10));
	}

	// Metodo multiplicacion
	public void testMult() {
		assertEquals(100, Operaciones.mult(10));
	}

	// Metodo div
	public void testDiv() {
		assertEquals(1, Operaciones.div(10));
	}

}